$(document).ready(function () {
  var lastScrollTop = 0;

  $(window).on("scroll", function () {
    var windowHeight = $(window).height();
    var scrollHeight = $(window).scrollTop();

    $(".fade-in").each(function () {
      var position = $(this).offset().top;
      if (position < scrollHeight + windowHeight && !$(this).hasClass("visible")) {
        $(this).addClass("visible").css("opacity", 0).animate({ opacity: 1 }, 500);
      } else if (position >= scrollHeight + windowHeight && $(this).hasClass("visible")) {
        $(this).removeClass("visible").css("opacity", 0);
      }
    });

    lastScrollTop = scrollHeight;
  });
});

//corazon
$(document).ready(function () {
  $(".likeable").on("click", function (event) {
    var imgContainer = $(this).parent();
    var clickCount = imgContainer.data("clicks") || 0;

    clickCount++;
    imgContainer.data("clicks", clickCount);

    
    var clickX = event.pageX - imgContainer.offset().left;
    var clickY = event.pageY - imgContainer.offset().top;

    var heart = $('<i class="fas fa-heart heart-icon" style="left: ' + clickX + 'px; top: ' + clickY + 'px;"></i>');
    imgContainer.append(heart);

    heart.css("transform", "translate(-50%, -50%) scale(" + (1 + clickCount * 0.1) + ")");

    // Movimiento del corazón con animación fluida
    heart.animate({ top: "-=30px" }, 500, "swing", function () {
      // Desvanecer el corazón después de 2 segundos
      setTimeout(function () {
        heart.fadeOut(500, function () {
          $(this).remove();
        });
      },);
    });
  });
});

// texto
$(document).ready(function () {
  var isFirstTime = true;
  var textContainer = $(".big-script p");
  var text = textContainer.text();
  textContainer.text("");

  function animateText() {
    var characters = text.split("");
    var delay = 100; 

    characters.forEach(function (char, index) {
      setTimeout(function () {
        textContainer.append(char);
      }, index * delay);
    });
  }


  $(window).on("scroll", function () {
    var windowHeight = $(window).height();
    var scrollHeight = $(window).scrollTop();

    $(".fade-in").each(function () {
      var position = $(this).offset().top;
      if (position < scrollHeight + windowHeight && !$(this).hasClass("visible")) {
        $(this).addClass("visible").css("opacity", 0).animate({ opacity: 1 }, 500);
      } else if (position >= scrollHeight + windowHeight && $(this).hasClass("visible")) {
        $(this).removeClass("visible").css("opacity", 0);
      }
    });

    if (isFirstTime && scrollHeight + windowHeight > textContainer.offset().top) {
      isFirstTime = false;
      animateText();
    }
  });
});

$(document).ready(function () {
  var currentIndex = 0;
  var items = $('.grid-item');
  var itemAmt = items.length / 1; 
  function cycleItems() {
    var windowWidth = $(window).width();
    if (windowWidth < 768) {
        itemsPerView = 1;
    } else {
        itemsPerView = 3;
    }
    var transformValue = 'translateX(' + (-currentIndex * 100 / itemsPerView) + '%)';
    $('.grid-container').css({
        'transform': transformValue,
        'transition': 'transform 0.5s ease-in-out'
    });
}
  function cycleItems() {
    var transformValue = 'translateX(' + (-currentIndex * 100 / 3) + '%)';
    $('.grid-items').css({
      'transform': transformValue,
      'transition': 'transform 0.5s ease-in-out'
    });
    items.removeClass('fadeOut').eq(currentIndex).addClass('fadeOut');
  }

  var autoSlide = setInterval(function () {
    currentIndex += 1;
    if (currentIndex > itemAmt - 1) {
      currentIndex = 0;
    }
    cycleItems();
  }, 3000);

  cycleItems();
});


$(document).ready(function () {
  $('.fa-heart').on('click', function () {
    $(this).toggleClass('active');
  });
});
$(document).ready(function () {
  $('.fa-bookmark').on('click', function () {
    $(this).toggleClass('active');
  });
});
